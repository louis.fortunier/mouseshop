# MouseShop
Développeur : Louis Fortunier et Tomy Nguyen

# Configuration Windows (cmd) :
cd ...
venv\Scripts\activate
set FLASK_APP=mouseshop
set FLASK_ENV=development
flask run

# Configuration Windows (PowerShell) :
cd ...
$env:FLASK_APP = "mouseshop"
$env:FLASK_ENV = "development"
flask run

# Cahier des charges :

Une boutique avec :
- Une page pour lister les produits
- Une page pour se connecter

Je ne regarde pas :
- Le HTML
- Le CSS
- Le JS

Je regarde :
- Le respect du guide de style (PEP8, vous pouvez utiliser Black)
- La simplicité et la lisibilité du code
- Les fonctionnalités

Fonctionnalités supplémentaires :
- Inscription
- Panier / Commandes
- Panneau administrateur : création de produits, suivi des commandes
- Panneau client : liste des commandes
- Gestion de stocks, de mode de livraison…
- Promotions
- …


# Configuration PyCharm :
target : C:...\mouseshop\run.py
Environnement variables : FLASK_APP=mouseshop

# Initialisation Database termial cmd :
(passer en venv = venv\Scripts\activate)
set FLASK_APP=mouseshop
flask init-db