import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from mouseshop.database import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')


@bp.route('/login', methods=('GET', 'POST'))
# Lorsque l'utilisateur se connecte
def login():
    if request.method == 'POST':
        username = request.form['email']
        password = request.form['password']
        db = get_db()
        error = None
        user = None
        if not username:
            error = 'L\'email est requis.'
        elif not password:
            error = 'Le mot de passe est requis.'
        else:
            user = db.execute(
                'SELECT * FROM user WHERE username = ?', (username,)
            ).fetchone()

        if user is None:
            print('user is none')
            error = 'Le mot de passe ou l\'email est erroné'

        if error is None:
            initSession(user['id'])
            return redirect(url_for('mouse.liste_mouse'))

        return render_template('auth/login.html', error=error)

    return render_template('auth/login.html')


@bp.route('/register', methods=('GET', 'POST'))
# Lorsque l'utilisateur s'enregistre
def register():
    if request.method == 'POST':
        username = request.form['email']
        password = request.form['password']
        is_admin = True
        if request.form.get('is_admin') is None:
            is_admin = False

        db = get_db()
        error = None

        if not username:
            error = 'L\'email est requis.'
        elif not password:
            error = 'Le mot de passe est requis.'
        else:
            user = db.execute(
                    'SELECT * FROM user WHERE username = ?', (username,)
            ).fetchone()

        if user is not None:
            error = 'L\'utilisateur {} est deja enregistrer.'.format(username)

        if error is None:
            db.cursor()
            db.execute(
                'INSERT INTO user (username, password, is_admin) VALUES (?, ?, ?)',
                (username, generate_password_hash(password), is_admin)
            )
            db.commit()
            u = db.execute (
                'SELECT * FROM user WHERE username = ?', (username,)
            ).fetchone()
            initSession(u['id'])

            return redirect(url_for('mouse.liste_mouse'))


        return render_template('auth/register.html', error=error)

    return render_template('auth/register.html')

def initSession(id):
    session.clear()
    session['user_id'] = id