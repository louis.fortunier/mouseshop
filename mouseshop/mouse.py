from flask import (
    Blueprint, render_template,
    request, redirect, url_for, session)

from mouseshop.database import get_db

bp = Blueprint('mouse', __name__, url_prefix='/mouse')


@bp.route('/listeMouse', methods=('GET', 'POST'))
def liste_mouse():
    if request.method == 'GET':
        db = get_db()
        mes_articles = db.execute(
            'SELECT * FROM article'
        )
        db.commit()
        user_id = session.get('user_id')
        user = db.execute(
            'SELECT * FROM user where id = ? ', (user_id,)
        ).fetchone()
        #print(user['is_admin'])
        print(user_id)
        if user is not None and user['is_admin'] == 1:
            return render_template('boutique/listeMouseAdmin.html', mes_articles=mes_articles)
        else:
            return render_template('boutique/listeMouse.html', mes_articles=mes_articles)





# Lorsque qu'on ajoute sans ID pour éviter d'avoir un ID obligatoire
@bp.route('/editMouse', defaults={'id_article': None} ,methods=('GET', 'POST'))

# Route avec paramètre
@bp.route('/editMouse/<int:id_article>', methods=('GET', 'POST'))

def edit_mouse(id_article):
    # Si on rentre dans la méthode GET
    if request.method == 'GET':
        article = type('obj', (object,), {'libelle': '','prix':''})
        db = get_db()
        articles = db.execute(
            'SELECT * FROM article where id = ? ',(id_article,)
        ).fetchall()
        if len(articles) == 1:
            article = articles[0]

        return render_template('boutique/ajoutMouse.html', article=article)

    # Si on rentre dans la méthode POST
    if request.method == 'POST':
        libelle = request.form['libelle']
        prix = request.form['prix']
        db = get_db()
        articles = db.execute(
            'SELECT * FROM article where id = ? ', (id_article,)
        ).fetchall()
        if len(articles) != 0:
            db.execute(
                'UPDATE article SET libelle = ?, prix = ? where id = ?', (libelle, prix, id_article)
            )
        else:
            db.execute(
                'INSERT INTO article (libelle, prix) VALUES (?,?)',
                (libelle, prix)
            )
        db.commit()
        return redirect(url_for('mouse.liste_mouse'))

    return render_template('boutique/ajoutMouse.html')

@bp.route('/delete/<int:id_article>', methods=['GET'])
def supprimer_mouse(id_article):
    db = get_db()
    db.execute(
        'DELETE FROM article WHERE id = ?', (id_article,)
    )
    db.commit()
    return redirect(url_for('mouse.liste_mouse'))
