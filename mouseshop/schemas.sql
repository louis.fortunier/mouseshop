DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS commande;
DROP TABLE IF EXISTS article;
DROP TABLE IF EXISTS ligne_commande;

CREATE TABLE user (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  username TEXT UNIQUE NOT NULL,
  password TEXT NOT NULL,
  is_admin BOOL NOT NULL
);

CREATE TABLE commande (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  user_id INTEGER NOT NULL,
  FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE article (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  libelle TEXT NOT NULL,
  prix INTEGER NOT NULL
);

CREATE TABLE ligne_commande (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  commande_id INTEGER NOT NULL,
  article_id INTEGER NOT NULL,
  FOREIGN KEY (commande_id) REFERENCES commande (id),
  FOREIGN KEY (article_id) REFERENCES article (id)
);

INSERT INTO user (username, password, is_admin) VALUES ('admin@admin.fr', 'admin', 1);
INSERT INTO user (username, password, is_admin) VALUES ('louis@louis.fr', 'louis', 0);
INSERT INTO user (username, password, is_admin) VALUES ('tomy@tomy.fr', 'tomy', 0)